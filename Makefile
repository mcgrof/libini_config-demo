all: demo

LDFLAGS := $(shell pkg-config --libs ini_config)
CCFLAGS := $(shell pkg-config --cflags ini_config) -Wall

demo: demo.c demo-0001.cfg
	gcc $(LDFLAGS) $(CCFLAGS) -o demo demo.c

clean:
	rm -f demo *.o
