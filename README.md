libini_config demo
==================

If you're looking to pick out a library for parsing a configuration file,
and your values are mostly unsigned int this may be of help. We'll provide
references to a few other libraries reviewed, and this repository contains a
demo to test libini_config against usigned int, and 64 bit unsigned int.

This stands to be the leading library given the community behind it, the
and the ability easily debug issues, and the fact that a lot of test code
exists against it.

The only thing currently making libekprofile is the size. libekprofile
is 14.86% the size libini_config.so.5.2.0. Refer to the URL for
libekprofile for details.

Other libraries
===============

Other libraries which have been reviewed:

  o libconfig: https://gitlab.com/mcgrof/libconfig-int64-issues
  o libekprofile: https://gitlab.com/mcgrof/libekprofile
