#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <ini_configobj.h>

static const char *config = "demo-0001.cfg";

#define DEMO_MAX_KEY		1024
#define DEMO_MAX_VALUE		PATH_MAX
#define DEMO_MAX_BUFFER		DEMO_MAX_KEY + DEMO_MAX_VALUE + 3

static int demo_check_file(const char *filename,
			   const struct stat *sb,
			   unsigned long long max_entries)
{
	unsigned long long size_limit;

	size_limit = DEMO_MAX_BUFFER * max_entries;

	printf("-------- Limits --------------\n");
	/*
	 * Detect wrap around -- if max_entries * size_limit goves over
	 * unsigned long long we detect tha there.
	 */
	if (size_limit < max_entries) {
		fprintf(stderr,
			"Cannot do sanity check as limit is beyond limit\n");
		return -E2BIG;
	}

	if (max_entries > INT_MAX) {
		printf("Must be doing a stress test -- limit is beyond what libini_config undersands\n");
	}

	printf("Max entries allowed %llu\n", max_entries);
	printf("File size limit: %llu bytes (~ %llu KiB, ~ %llu MiB, ~ %llu GiB\n",
	       size_limit,
	       size_limit / 1024,
	       size_limit / 1024 / 1024,
	       size_limit / 1024 / 1024 / 1024);

	printf("File size: %llu bytes (~ %llu KiB, ~ %llu MiB, ~ %llu GiB)\n",
	       (unsigned long long) sb->st_size,
	       (unsigned long long) sb->st_size / 1024,
	       (unsigned long long) sb->st_size / 1024 / 1024,
	       (unsigned long long) sb->st_size / 1024 / 1024 / 1024);

	if (sb->st_size > size_limit) {
		fprintf(stderr,
			"File %s is too big! Max entries expected: %llu\n",
			filename, max_entries);
		return -E2BIG;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	struct ini_cfgobj *ini_config = NULL;
	struct ini_cfgfile *ctx = NULL;
	int ret;
	const struct stat *sp = NULL;
	unsigned long long max_entries = 1000;
	char **sections, **attrs;
	char *section, *attr;
	unsigned int i = 0, x = 0;
	int size, num_attrs;

	ret = ini_config_create(&ini_config);
	if (ret)
		return ret;

	ret = ini_config_file_open(config, INI_META_STATS, &ctx);
	if (ret) {
		fprintf(stderr, "ini_config_file_open(): could not open config file: %s: %s\n",
			config, strerror(ret));
		goto out;
	}

	sp = ini_config_get_stat(ctx);
	if (!sp) {
		ret = -EACCES;
		fprintf(stderr, "ini_config_get_stat(): could get stat on file: %s: %s\n",
			config, strerror(ret));
		goto out;
	}

	printf("Your system limits.h says:\n\n");
	printf("INT_MAX: %u\n", INT_MAX);
	printf("UINT_MAX: %u\n", UINT_MAX);
	printf("LONG_MAX: %lu\n", LONG_MAX);
	printf("ULLONG_MAX: %llu\n", ULLONG_MAX);
	printf("\n");

	ret = demo_check_file(config, sp, max_entries);
	if (ret) {
		fprintf(stderr, "demo_check_file() failed\n");
		goto out;
	}

	printf("demo_check_file() OK!\n");

	/* Stop on any errors detected */
	ret = ini_config_parse(ctx,
			       INI_STOP_ON_ANY,
			       0,
			       0,
			       ini_config);
	if (ret) {
		fprintf(stderr, "Error parsing config file\n");
		goto out;
	}

	/* after parsing get the section list */
	/* ini_config is struct ini_cfgobj */
	sections = ini_get_section_list(ini_config, &size, &ret);
	if (ret || sections == NULL) {
		fprintf(stderr, "Could not get sections\n");
		goto out;
	}

	printf("Number of sections: %d\n", size);
	printf("Parsing file:\n\n");
	printf("----------------------------------------------------------\n");
	for (i=0; i < size; i++) {
		if (!sections[i]) {
			printf("BUG on %i\n", i);
			break;
		}
		section = sections[i];
		printf("%s\n", section);
		printf("=======================\n");
		attrs = ini_get_attribute_list(ini_config, section,
					       &num_attrs, &ret);
		if (ret || attrs == NULL) {
			fprintf(stderr, "Could not get attrs\n");
			goto out_free_sections;
		}

		for (x=0; x < num_attrs; x++) {
			uint64_t val;
			struct value_obj *vo;
			const char *str;

			attr = attrs[x];
			ret = ini_get_config_valueobj(section, attr, ini_config,
						      INI_GET_FIRST_VALUE, &vo);
			if (ret || !vo) {
				fprintf(stderr, "Error when fetching object for attribute: \"%s\"\n",
					attr);
				continue;
			}

			//val = ini_get_uint64_config_value(vo, 1, 0, &ret);
			val = ini_get_unsigned_config_value(vo, 1, 0, &ret);
			if (ret) {
				fprintf(stderr, "Error when converting attr \"%s\" to uint64, ret: %d\n",
					attr, ret);
				continue;
			}
			str = ini_get_const_string_config_value(vo, &ret);
			if (ret || !str)
				continue;

			printf("%s: %lu (\"%s\")\n", attr, val, str);
		}
		printf("-------------------------\n");
		ini_free_attribute_list(attrs);
	}

out_free_sections:
	ini_free_section_list(sections);

	//ini_config_file_print(ctx);

	// ini_get_long_config_value()
	// ini_get_ulong_config_value()
	// ini_get_uint64_config_value()
	// ini_get_int64_config_value()
	// ini_get_string_config_value()
	// ini_get_const_string_config_value()

out:
	ini_config_destroy(ini_config);
	return ret;
}
